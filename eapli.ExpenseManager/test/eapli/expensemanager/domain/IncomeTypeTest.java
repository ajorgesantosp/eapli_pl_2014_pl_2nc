/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Classe teste para a classe IncomeType
 *
 * @author Joel
 */
public class IncomeTypeTest {

    public IncomeTypeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Teste do construtor da classe IncomeType
     */
    @Test(expected = IllegalArgumentException.class)
    public void testIncomeType() {
        // TODO review the generated test code and remove the default call to fail.
        IncomeType teste = null;
        IncomeType testNull = new IncomeType(null);
        IncomeType testEmpty = new IncomeType("");
    }

}
