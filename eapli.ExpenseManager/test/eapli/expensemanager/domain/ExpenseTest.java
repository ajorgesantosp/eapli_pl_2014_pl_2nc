/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.domain;

import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mak3r
 */
public class ExpenseTest {
    
    public ExpenseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getDate method, of class Expense.
     */
//    @Test
//    public void testGetDate() {
//        System.out.println("getDate");
//        Expense instance = null;
//        Calendar expResult = null;
//        Calendar result = instance.getDate();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//       // fail("The test case is a prototype.");
//    }

    /**
     * Test of getMonthFromExpense method, of class Expense.
     */
    @Test
    public void testGetMonthFromExpense() {
        System.out.println("getMonthFromExpense");
        Expense instance = null;
        int expResult = 0;
        int result = instance.getMonthFromExpense();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
    //tests if input value of "amount" is negative
    @Test (expected = IllegalArgumentException.class)  
    public void testNegativeValueFromExpense(){
        String desc ="zdfdsfs";
        double val = -1.0;
        Calendar date = Calendar.getInstance();
        Expense instance = new Expense(desc,date,val);
    }
    
    //tests if input value of "amount" is zero
    @Test (expected = IllegalArgumentException.class)  
    public void testZeroValueFromExpense(){
        String desc ="zdfdsfs";
        double val = 0.0;
        Calendar date = Calendar.getInstance();
        Expense instance = new Expense(desc,date,val);
    }
    
    //tests if input value of "description" is null or empty
    @Test (expected = IllegalArgumentException.class)  
    public void testNoDescFromExpense(){
        String desc ="";
        double val = 1.0;
        Calendar date = Calendar.getInstance();
        Expense instance = new Expense(desc,date,val);
    }
    
}
