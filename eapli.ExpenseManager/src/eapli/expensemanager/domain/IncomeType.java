/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import eapli.util.Validations;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Classe que representa as receitas.
 *
 * @author Joel
 */
@Entity
public class IncomeType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idIncomeType;
    private String strIncomeType;

    /**
     * Construtor da IncomeType.
     *
     * @param incomeType string com nome da receita
     */
    public IncomeType(final String incomeType) {
        if (Validations.isNullOrEmpty(incomeType)) {
            throw new IllegalArgumentException();
        }
        this.strIncomeType = incomeType;
    }

    /**
     * Construtor por defeito. Uso obrigatorio com o JPA.
     */
    public IncomeType() {

    }
}
