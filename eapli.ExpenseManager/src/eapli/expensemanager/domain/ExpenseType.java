/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import eapli.util.Validations;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Entity
public class ExpenseType {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idExpenseType;
    private String text;

	public ExpenseType(String text) {
		
//		if (Validations.isNullOrEmpty(text) ) {
//			throw new IllegalArgumentException();
//		}
		this.text = text;
	}
        
        public ExpenseType(){
            
        }

}
