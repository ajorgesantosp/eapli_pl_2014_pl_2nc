/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author José Marques
 */
@Entity
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPayment;
    private PaymentMean pm;
    private String description;

    public Payment(PaymentMean pm) {
        this.pm = pm;
    }
    
    public Payment(){};

    /**
     * @return the pm
     */
    public PaymentMean getPm() {
        return pm;
    }
}
