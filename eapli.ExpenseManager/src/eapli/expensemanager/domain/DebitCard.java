/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author Vitor
 */
@Entity
@DiscriminatorValue("DC") 
public class DebitCard extends PaymentCard {

    public DebitCard() {
    }

    public DebitCard(String banckName, String cardNumber) {
        super(banckName, cardNumber);
    }

    @Override
    public String toString() {
        return String.format("Debit card: %s", super.toString());
    }

}
