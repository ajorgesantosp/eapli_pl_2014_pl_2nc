/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import eapli.util.Validations;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author Vitor
 */
@Entity
@DiscriminatorValue("CH") 
public class Check extends PaymentMean {

    private String bankNameAcronym;
    private String bankName;


    public Check() {
    }

    public Check(String bankNameAcronym, String bankName) {
        if (Validations.isNullOrEmpty(bankNameAcronym) || Validations.isNullOrEmpty(bankName)) {
            throw new IllegalArgumentException();
        }
        this.bankNameAcronym = bankNameAcronym;
        this.bankName = bankName;
    }

    @Override
    public String toString() {
        return String.format("Bank name acronym: %s - Bank name: %s", bankNameAcronym, bankName);
    }
}
