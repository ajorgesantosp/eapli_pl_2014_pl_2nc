/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Persistence;

/**
 *
 * @author Mak3r Class that contains all the expenses and future profits(?) of a
 * specific month Class also handles all the model manipulation for the UCs
 * related with sheets
 */
@Entity
public class MonthlySheet implements Serializable {

    //TODO: adicionar as variaveis necessarias a outros casos de uso: saldos / receitas
    //list containing all the expense of the current month

    @Id
    @GeneratedValue  //(strategy = GenerationType.IDENTITY)
    private long surrogatekey;
       
    //@Id
    //@GeneratedValue
    private int idMonthlySheet;

    @OneToMany(cascade = CascadeType.ALL)
    private final List<Expense> expenseList = new ArrayList<Expense>();

    @OneToMany(cascade = CascadeType.ALL)
    private final List<Income> incomeList = new ArrayList<Income>();

    //variable that contains current month for easier search of correct sheet by the book
    private int mnth;

    /**
     * empty constructor
     */
    public MonthlySheet() {

    }

    /**
     * Constructor that receives a month and saves it
     *
     * @param month
     */
    public MonthlySheet(int month) {
        this.mnth = month;
    }

    /**
     * getter to search sheets by month
     *
     * @return integer with number of month
     */
    public int getMonthFromSheets() {
        return this.mnth;
    }

    /**
     * method called by controller to add new expenses to current current
     *
     * @param expense
     */
    public void registerExpense(Expense expense) {
        saveExpense(expense);
    }

    /**
     * private method that adds expense to sheet
     *
     * @param expense
     */
    private void saveExpense(Expense expense) {
        if (expense == null) {
            throw new IllegalArgumentException();
        }

        expenseList.add(expense);
//                for(Expense e: expenseList){
//                    //debug
//                    System.out.println("lista de despesas em que o mes é: "+e.getMonthFromExpense());
//                }
    }

    public void deleteList() {
        
        expenseList.clear();
        
    }
    
    public void registerIncome(Income income) {
        if (income == null) {
            throw new IllegalArgumentException();
        }

        incomeList.add(income);
//        for (Income i : incomeList) {
//            Calendar date = i.getDate();
//            System.out.println("lista de despesas " + date.get(Calendar.MONTH));
//        }

    }



    /* method created for UC05 - MonthlyExpenses */
    public double getMonthlyExpenses() {
        double amount = 0;
        for (Expense e : expenseList) {
            amount = amount + e.getValue();
        }
        return amount;
    }

//    public void registerExpenseJPA(Expense expense) {
//        EntityManagerFactory factory = Persistence.
//                createEntityManagerFactory("eapli.ExpenseManagerPU");
//        EntityManager manager = factory.createEntityManager();
//
//        manager.getTransaction().begin();
//        manager.persist(expense);
//        manager.getTransaction().commit();
//        manager.close();
//    }
    
    //metodo toString para ver o que contem as folhas
//    @Override
//    public String toString() {
//        return "-";
//    }
    
    public double getWeekExpenses(Calendar initialDate, Calendar finalDate) {
        double amount = 0.0;
        for (Expense e : expenseList) {
            if (e.isInDate(initialDate, finalDate)) {
                amount += e.getValue();
            }
        }
        return amount;
    }
    
}
