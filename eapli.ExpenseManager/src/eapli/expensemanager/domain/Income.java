/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import eapli.util.Validations;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author sdcastro
 */
@Entity
public class Income {

     @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idIncome;
    private String description;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar m_date;
    private double m_value;
   
    private int m_month;
    /**
     *  JPA obriga a construtor sem parametros
     */
    public Income(){
        
    }
    
   
        /**
     * constructor of Income class
     * @param description description of the income
     * @param date date income was made
     * @param value  amount of income
     * internal month parameter to easily handle searches by month
     */
    
    public Income(String description, Calendar date, double value) {
        if (Validations.isNullOrEmpty(description)) {
            throw new IllegalArgumentException("Description cannot be empty.");
        } 
        else if (value <= 0) {
            throw new IllegalArgumentException("Value must be positive.");        
        }
        
        this.description = description;
        this.m_date = date;
        this.m_value = value;
        this.m_month = (date.get(Calendar.MONTH)+1);
    }

    public String getDescription() {
        return description;
    }

    public Calendar getDate() {
        return m_date;
    }

    
    public int getMonthFromIncome(){
        return m_month;
    }
    
    @Override
    public String toString() {
        return description + " :: " + m_date.toString() + " :: "
                + String.valueOf(m_value);
    }

}
