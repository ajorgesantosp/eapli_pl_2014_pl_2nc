/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author Vitor
 */
@Entity
@DiscriminatorValue("CC") 
public class CreditCard extends PaymentCard implements Serializable {

    public CreditCard() {
    }

    public CreditCard(String bankName, String cardNumber) {
        super(bankName, cardNumber);
    }

    @Override
    public String toString() {
        return String.format("Credit card: %s", super.toString());
    }
}
