/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import eapli.util.Validations;
import java.io.Serializable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;



/**
 *
 * @author Vitor
 */

@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="PM")

public abstract class PaymentMean implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPaymentMean;
    
    public PaymentMean() {

    }

    /**
     * @return the idPaymentType
     */
    public int getIdPaymentMean() {
        return idPaymentMean;
    }

    /**
     * @param idPaymentType the idPaymentType to set
     */
    public void setIdPaymentMean(int idPaymentType) {
        this.idPaymentMean = idPaymentType;
    }

}
