    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.domain;

import eapli.util.Validations;
import java.io.Serializable;
import javax.persistence.Entity;

/**
 *
 * @author Vitor
 */
@Entity
public abstract class PaymentCard extends PaymentMean implements Serializable{
    
    
    private String cardName;
    private String cardNumber;
    
    public PaymentCard() {
    }
    
    /**
     * 
     * @param bankName
     * @param cardNumber 
     */
    public PaymentCard(String bankName, String cardNumber) {
        if (Validations.isNullOrEmpty(bankName) || Validations.isNullOrEmpty(cardNumber)) {
            throw new IllegalArgumentException();
        }
        this.cardName = bankName;
        this.cardNumber = cardNumber;
    }
    
    public String toString() {
        return String.format("Card name: %s - Card number: %s",cardName , cardNumber);
    }
    
}