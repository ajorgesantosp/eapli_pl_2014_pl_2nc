/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.domain;

import eapli.util.Validations;
import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Temporal;

/**
 *
 * @author Mak3r
 * Object class that contains all the info about expenses
 */
@Entity
public class Expense implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idExpense;
    private String description;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar date;
    private double value;
    private int month;
    @OneToOne(cascade = CascadeType.ALL)
    private Payment pm;
    /**
     * Empty constructor created because JPA demands so
     */
    public Expense(){
        
    }
    
    /**
     * constructor of expense class
     * @param expenseDesc saves the description of the expense
     * @param expenseDate saves the date the expense was made
     * @param expenseValue  saves the amount spent
     * internal month parameter to easily handle searches by month
     */
    public Expense(String expenseDesc, Calendar expenseDate, double expenseValue) {
        if (Validations.isNullOrEmpty(expenseDesc) ) {
			throw new IllegalArgumentException("expense description cannot be empty");
		}
        
        if(expenseValue<=0.0){
            throw new IllegalArgumentException("Value cannot be 0 nor negative");
        }
        
        if((expenseDate.get(Calendar.MONTH)+1)>12 ||(expenseDate.get(Calendar.MONTH)+1)<1){
            //never used - values are always converted to the right interval (1-12)
            throw new IllegalArgumentException("Month number must be between 1 and 12");
        }
        this.description = expenseDesc;
        this.date = expenseDate;
        this.value = expenseValue;
        this.month = (expenseDate.get(Calendar.MONTH)+1);
        
    }
    
    /**
     * 
     * @return the parameter date of the object
     */
    public Calendar getDate(){
        return date;
    }
    /*method created for UC05 - MonthlyExpenses*/
    public double getValue(){
        return value;
    }
    
    public int getMonthFromExpense(){
        return month;
    }

    
    /**
    Valida se despesa encontra-se em determinado periodo.
    */
    public boolean isInDate(Calendar from, Calendar to) {
        return (this.date.after(from)&& this.date.before(to));
    }
    



}