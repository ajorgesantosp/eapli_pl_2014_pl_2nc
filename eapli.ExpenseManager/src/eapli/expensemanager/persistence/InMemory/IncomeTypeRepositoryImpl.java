/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.persistence.InMemory;

import eapli.expensemanager.domain.IncomeType;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author i101153
 */
public class IncomeTypeRepositoryImpl implements eapli.expensemanager.persistence.IIncomeTypeRepository {
    
        /**
     * Lista onde são guardadas as receitas
     */
    private static final List<IncomeType> data = new ArrayList<IncomeType>();

    /**
     * Guarda o tipo de receita numa lista de tipos de receitas
     *
     * @param incomeType instancia de um IncomeType para guardar na lista
     */
    @Override
    public void save(IncomeType incomeType) {
        if (incomeType == null) {
            throw new IllegalArgumentException();
        }
        data.add(incomeType);
    }

    /**
     * Devolve o tamanho da lista de IncomeType
     *
     * @return numero de elementos da lista de IncomeType
     */
//    @Override
//    public int count() {
//        return data.size();
//    }
    
}
