package eapli.expensemanager.persistence.InMemory;

import eapli.expensemanager.domain.PaymentMean;
import java.util.ArrayList;

/**
 *
 * @author José Marques (1100910)
 */
public class PaymentMeanRepositoryImpl implements eapli.expensemanager.persistence.IPaymentMeanRepository {

    /**
     * Lista onde são guardados os meios de pagamento
     */
    private static ArrayList<PaymentMean> carteira = new ArrayList<PaymentMean>();

    /**
     * Adiciona um meio de pagamento à carteira
     */
    @Override
    public void addPaymentMean(PaymentMean pm) {
        if (pm == null) {
            throw new IllegalArgumentException();
        }
        carteira.add(pm);
    }

    /**
     * Devolve um ArrayList com todos os tipos de pagamento
     */
    @Override
    public ArrayList<PaymentMean> retrievePaymentMeans() {
        return new ArrayList<PaymentMean>(carteira);
    }
}
