/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence.InMemory;

import eapli.expensemanager.domain.ExpenseType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ExpenseTypeRepositoryImpl implements eapli.expensemanager.persistence.IExpenseTypeRepository {

	private static final List<ExpenseType> data = new ArrayList<ExpenseType>();

        
        @Override
	public void save(ExpenseType expenseType) {
		if (expenseType == null) {
			throw new IllegalArgumentException();
		}

		data.add(expenseType);
	}

	public int count() {
		return data.size();
	}
}
