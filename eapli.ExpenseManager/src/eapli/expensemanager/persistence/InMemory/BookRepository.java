/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence.InMemory;

import eapli.expensemanager.domain.ExpenseType;
import eapli.expensemanager.domain.MonthlySheet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mak3r
 */
public class BookRepository implements eapli.expensemanager.persistence.IBookRepository {

    private static final List<MonthlySheet> sheets = new ArrayList<MonthlySheet>();
    //private static final List<ExpenseType> data = new ArrayList<ExpenseType>();

    @Override
    public void save(MonthlySheet monthSheet) {
        if (monthSheet == null) {
            throw new IllegalArgumentException();
        }

        sheets.add(monthSheet);
    }

    public int count() {
        return sheets.size();
    }

    public MonthlySheet findSheet(int month){
        for(MonthlySheet iterator : sheets){
            if(iterator.getMonthFromSheets()==month){
                return iterator;
            }
        }
        MonthlySheet sheet = new MonthlySheet(month);
        return sheet;
    }

}
