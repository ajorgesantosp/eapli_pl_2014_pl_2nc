package eapli.expensemanager.persistence;

import eapli.expensemanager.domain.PaymentMean;
import java.util.ArrayList;

/**
 *
 * @author José Marques
 */
public interface IPaymentMeanRepository {
    public void addPaymentMean(PaymentMean pm);
    public ArrayList<PaymentMean> retrievePaymentMeans();
    
    //@José Marques: Verificar se faz sentido incluir o método abaixo no interface
    //public ArrayList<PaymentMean> retrievePaymentMeans();
}
