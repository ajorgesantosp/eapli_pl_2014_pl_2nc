/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.persistence;

import eapli.expensemanager.domain.MonthlySheet;

/**
 *
 * @author Mak3r
 */
public interface IBookRepository {

    public void save(MonthlySheet monthSheet);

    public int count();

    //public void findSheet();
}
