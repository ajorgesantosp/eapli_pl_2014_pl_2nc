/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence;

import eapli.expensemanager.persistence.InMemory.InMemoryRepositoryFactory;
import java.io.FileInputStream;
import java.util.Properties;


/**
 *
 * @author s3rgio.
 */
public class PersistenceFactory {

    private static PersistenceFactory instance = new PersistenceFactory();
    private IRepositoryFactory myFactory;
       
    private PersistenceFactory() {
        myFactory = loadRepositoryFactoryFromFile();
    }
                    
    public static PersistenceFactory getInstance() {        
        return instance;
    }

    public IRepositoryFactory getRepositoryFactory() {
        return myFactory;
    }

    private static IRepositoryFactory loadRepositoryFactoryFromFile() {
                       
        Properties persistence = new Properties();
        String PROPERTIES_PATH = System.getProperty("user.dir")+"/config/persistence.properties";
        //System.out.println(PROPERTIES_PATH);
        try {
            persistence.load(new FileInputStream(PROPERTIES_PATH));
            String property = PersistenceFactory.class.getSimpleName() + ".useFactory";
            Class factoryClass = Class.forName(persistence.getProperty(property));
            return (IRepositoryFactory) factoryClass.newInstance();
        } catch (Exception e) {
            System.err.println("Unable to load persistence factory");
            e.printStackTrace(System.err);
            System.err.println("Reverting to Memory");
            return new InMemoryRepositoryFactory();
        }
    }
}
