
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence;


/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public interface IRepositoryFactory {

    IExpenseTypeRepository getExpenseTypeRepository();
    IMonthlySheetRepository getMonthlySheetRepository();
    IIncomeTypeRepository getIncomeTypeRepository();    
    IPaymentMeanRepository getPaymentMeanRepository();
    
}
 


/*
    

    
    public BookRepository getBookRepository() {
        return new BookRepository();
    }


}
*/
