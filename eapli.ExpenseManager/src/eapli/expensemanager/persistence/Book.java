
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.persistence;

import eapli.util.DateTime;
import eapli.expensemanager.domain.Expense;
import eapli.expensemanager.domain.Income;
import eapli.expensemanager.domain.MonthlySheet;
import eapli.expensemanager.persistence.InMemory.BookRepository;
import eapli.expensemanager.persistence.jpa.MonthlySheetRepositoryJPA;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Model and persistence class (at this moment) that saves and manipulates all the sheets
 * @author Mak3r
 */
public class Book {
    
    //private static final List<MonthlySheet> sheets = new ArrayList<MonthlySheet>();// TO BE REMOVED - in persistence 
    
   
    /**
     * method created for UC03 - register expense
     * checks if the sheet exists, returning this one or creating a new sheet with the new month
     * @param expense to get the date and find the correct sheet
     * @return the sheet of the wanted month
     * NO LONGER USED - used ByMonth
     */
//    public MonthlySheet findMonthlySheet(Expense expense){
//        Calendar tmpMonth = expense.getDate();
//        
//        //int mes = (expenseDate.get(expenseDate.MONTH));
//        
//        for(MonthlySheet iterator : sheets){
//            if(iterator.getMonthFromSheets()==tmpMonth.MONTH){
//                //debug
//                System.out.println("mes dentro for >>>>>>  "+iterator.getMonthFromSheets());
//                return iterator;
//            }
//            
//        }
//        MonthlySheet sheet = new MonthlySheet(tmpMonth.MONTH);
//        saveSheet(sheet);
//        return sheet;
//    }
    /**
     * method added to replace previous one,dealing with int month instead of Calendar type
     * @param month integer that contains month
     * PREPARADO PARA PERSISTENCIA EM MEMORIA
     * @return 
     * -------------------------------------------------------
     * NOT IN USE - BOOK DESIGN IS NOT IMPLEMENTED
     * -------------------------------------------------------
     */

    
    /*
    public MonthlySheet findMonthlySheetByMonth(int month) {
       //PREPARADO PARA BOOK, TERÁ QUE SER ALTERADO PARA SHEETS?
        //factory pattern
        RepositoryFactory repoFactory = new RepositoryFactory();
        
        BookRepository repo = repoFactory.getBookRepository();
        
        MonthlySheet sheet = repo.findSheet(month);
      
        repo.save(sheet);
      
        return sheet;
    }
    
    public MonthlySheet findMonthlySheetByMonthJPA(int month){
        
        RepositoryFactory repoFactory = new RepositoryFactory();
        
        MonthlySheetRepositoryJPA repo = repoFactory.getMonthlySheetRepository();
        
        MonthlySheet sheet = repo.findSheet(month);
      
        return sheet;
        
    }
    
*/

    /**
     * private method that adds sheet to the list of monthlysheets
     * possibly to be designed in the book-persistence class instead of this model one
     * @param sheet 
     */
//    private void saveSheet(MonthlySheet sheet) {
//        if (sheet == null) {
//			throw new IllegalArgumentException();
//		}
//
//		//sheets.add(sheet);
//    }


    /*
    method created for UC11 - register income
    checks if the sheet exists, returning this one or creating a new sheet with the new month
    */
    
/*
                                    //-----------------------TODO: Alterar codigo UC11 para funcionar com persistencia---------------------------
    public MonthlySheet findMonthlySheet(Income income){
        //TODO: NOT WORKING, JUST TO BE ERROR FREE
        MonthlySheet sheet = new MonthlySheet();
        return sheet;
    }
    
   */
//   
//    public MonthlySheet findMonthlySheet(Income income){
//        Calendar date = income.getDate();
//         
//        /*  returns existing sheet */
//        for(MonthlySheet ms : sheets){
//            if(ms.getMonthFromSheets()== date.get(Calendar.MONTH)){
//                return ms;
//            }
//            
//        }
//        /*  sheet doesn't exist; returns new one */
//        MonthlySheet sheet = new MonthlySheet(date.get(Calendar.MONTH));
//        return sheet;
//    }
   
    
    
    /*
    method created for UC05 - MonthlyExpenses
    get the current monthlysheet
    */
   
                    //-----------------------TODO: Alterar codigo UC5 para funcionar com persistencia---------------------------
    public  double findMonthlySheet(int actualMonth){
        //TODO: NOT WORKING, JUST TO BE ERROR FREE
        return actualMonth;
    }
//    
//     public  double findMonthlySheet(int actualMonth){
//        double amount=0;
//        /*  gets  sheet of month */
//        for(MonthlySheet ms : sheets){
//            if(ms.getMonthFromSheets() == actualMonth){
//                amount = CalculateMonthlyExpenses(ms);
//            }   
//        }
//      return amount;
//    }
     /*
    method created for UC05 - MonthExpenses
    Calculate the total of the month expenses
    */
    /*
    
     private double CalculateMonthlyExpenses(MonthlySheet sheet) 
     {
         double amount;
         amount = sheet.getMonthlyExpenses();
         return amount;
     }
        
    */

}