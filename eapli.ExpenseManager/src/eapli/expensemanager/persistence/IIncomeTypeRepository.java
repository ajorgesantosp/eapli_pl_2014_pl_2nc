/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence;

import eapli.expensemanager.domain.IncomeType;


/**
 * Interface do Repositorio
 *
 * @author Joel
 */
public interface IIncomeTypeRepository {

    	public void save(IncomeType incomeType);

	//public int count();
    
}
