/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.persistence.jpa;

import eapli.expensemanager.domain.IncomeType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author i101153
 */
public class IncomeTypeRepositoryJPA implements eapli.expensemanager.persistence.IIncomeTypeRepository{
    
        public void save(IncomeType incomeType) {
        
            /*
            Testa se valor introduzido é nulo
            */
            if (incomeType == null) {
                throw new IllegalArgumentException();
            }
        
            EntityManagerFactory factory = Persistence.createEntityManagerFactory("eapli.ExpenseManagerPU");
            EntityManager manager = factory.createEntityManager();

            manager.getTransaction().begin();
            manager.persist(incomeType);
            manager.getTransaction().commit();
            manager.close();
    }

   
    
}
