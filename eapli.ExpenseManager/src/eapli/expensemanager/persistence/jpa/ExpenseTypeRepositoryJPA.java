/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence.jpa;

import eapli.expensemanager.domain.ExpenseType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author dei
 */
public class ExpenseTypeRepositoryJPA implements eapli.expensemanager.persistence.IExpenseTypeRepository {

        @Override
	public void save(ExpenseType expenseType) {
		if (expenseType == null) {
			throw new IllegalArgumentException();
		}
		EntityManagerFactory factory = Persistence.
			createEntityManagerFactory("eapli.ExpenseManagerPU");
		EntityManager manager = factory.createEntityManager();

		manager.getTransaction().begin();
		manager.persist(expenseType);
		manager.getTransaction().commit();
		manager.close();

	}
}
