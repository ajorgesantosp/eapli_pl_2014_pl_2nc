package eapli.expensemanager.persistence.jpa;


import eapli.expensemanager.domain.ExpenseType;
import eapli.expensemanager.domain.PaymentMean;
import eapli.expensemanager.persistence.InMemory.*;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author José Marques (1100910)
 */
public class PaymentMeanRepositoryJPA implements eapli.expensemanager.persistence.IPaymentMeanRepository {

    /**
     * Lista onde são guardados os meios de pagamento
     */
    private static ArrayList<PaymentMean> carteira = new ArrayList<PaymentMean>();

    /**
     * Adiciona um meio de pagamento à carteira
     */
    @Override
    public void addPaymentMean(PaymentMean pm) {
        if (pm == null) {
            throw new IllegalArgumentException();
        }

        EntityManagerFactory factory = Persistence.
                createEntityManagerFactory("eapli.ExpenseManagerPU");
        EntityManager manager = factory.createEntityManager();

        manager.getTransaction().begin();
        manager.persist(carteira.add(pm));
        manager.getTransaction().commit();
        manager.close();

    }

    public void save(PaymentMean pm) {
        if (pm == null) {
            throw new IllegalArgumentException();
        }
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("eapli.ExpenseManagerPU");
        EntityManager manager = factory.createEntityManager();

        manager.getTransaction().begin();
        manager.persist(pm);
        manager.getTransaction().commit();
        manager.close();
    }

    /**
     * Devolve um ArrayList com todos os tipos de pagamento
     */
    @Override
    public ArrayList<PaymentMean> retrievePaymentMeans() {
        EntityManagerFactory factory = Persistence.
			createEntityManagerFactory("eapli.ExpenseManagerPU");
        EntityManager manager = factory.createEntityManager();
        Query q = manager.createQuery("SELECT x from PaymentMean x");
        return new ArrayList<PaymentMean>(q.getResultList());
    }
}
