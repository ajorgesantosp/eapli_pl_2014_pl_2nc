
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence.jpa;

import eapli.expensemanager.domain.MonthlySheet;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author Mak3r
 */
public class MonthlySheetRepositoryJPA implements eapli.expensemanager.persistence.IMonthlySheetRepository {

    @Override
    public void save(MonthlySheet monthSheet) {
        if (monthSheet == null) {
            throw new IllegalArgumentException();
        }
        
        EntityManagerFactory factory = Persistence.
                createEntityManagerFactory("eapli.ExpenseManagerPU");
        EntityManager manager = factory.createEntityManager();

        try {
            manager.getTransaction().begin();
            manager.persist(monthSheet);
            manager.getTransaction().commit();
        }  catch (PersistenceException ex) { 
            manager.getTransaction().begin();
            manager.merge(monthSheet);
            manager.getTransaction().commit();
        }
         finally {
            manager.close();
        }
    }
    
    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * method created to access persistence by JPA and find or create
     * monthlySheet
     *
     * @param month
     * @return new monthlySheet or existing one to be updated
     */
    @Override
    public MonthlySheet findSheet(int month) {
        //anotaçao - query a base de dados, SELECT x from MonthlySheet x WHERE x.month = :param
        EntityManagerFactory factory = Persistence.
                createEntityManagerFactory("eapli.ExpenseManagerPU");
        EntityManager manager = factory.createEntityManager();

        //--------- query mais eficiente - nao implementada ainda -TESTAR
        //Query q1 = manager.createQuery("SELECT x FROM MonthlySheet x WHERE x.mnth=:mes");       
        //q1.setParameter("mes",month);
        
        Query q = manager.createQuery("SELECT x from MonthlySheet x");//TESTE DE QUERY - FUNCIONA
        List<MonthlySheet> results = q.getResultList();
        MonthlySheet returnable = new MonthlySheet(month);
        for (MonthlySheet iterator : results) {
            //debug
            //System.out.println("lista de despesas de folhas na BD - mes é: "+iterator.getMonthFromSheets());
            if (results.isEmpty()) {    //never enters here?
                return returnable;
            } else {
                if (iterator.getMonthFromSheets() == month) {
                    //folha existe, retorna essa folha
                    iterator.deleteList();
                    return iterator;
                }
            }
        }
        return returnable;
    }

    
    public double calculateWeekExpenses() {

        double totalWeekExpenses = 0.0;

        Calendar initialWeekDate = DateTime.beginningOfWeek(DateTime.currentYear(), DateTime.currentWeekNumber());
        Calendar finalWeekDate = DateTime.endOfWeek(DateTime.currentYear(), DateTime.currentWeekNumber());

        if (initialWeekDate.get(Calendar.MONTH) == finalWeekDate.get(Calendar.MONTH)) {
            MonthlySheet month = findSheet(initialWeekDate.get(Calendar.MONTH));
            totalWeekExpenses = month.getWeekExpenses(initialWeekDate, finalWeekDate);
        } else {
            MonthlySheet initialMonth = findSheet(initialWeekDate.get(Calendar.MONTH));
            MonthlySheet finalMonth = findSheet(finalWeekDate.get(Calendar.MONTH));
            totalWeekExpenses = initialMonth.getWeekExpenses(initialWeekDate, DateTime.endOfMonth(DateTime.currentYear(), initialWeekDate.get(Calendar.MONTH)));
            totalWeekExpenses += finalMonth.getWeekExpenses(initialWeekDate, finalWeekDate);
        }
        return totalWeekExpenses;
    }

}
