/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence.jpa;


import eapli.expensemanager.persistence.IExpenseTypeRepository;
import eapli.expensemanager.persistence.IIncomeTypeRepository;
import eapli.expensemanager.persistence.IMonthlySheetRepository;
import eapli.expensemanager.persistence.IPaymentMeanRepository;
import eapli.expensemanager.persistence.IRepositoryFactory;

/**
 *
 * @author s3rgio
 */
public class RepositoryFactoryJPA implements IRepositoryFactory {


    @Override
    public IExpenseTypeRepository getExpenseTypeRepository() {
        return new ExpenseTypeRepositoryJPA();
    }

    @Override
    public IMonthlySheetRepository getMonthlySheetRepository() {
        return new MonthlySheetRepositoryJPA();
    }

    @Override
    public IIncomeTypeRepository getIncomeTypeRepository() {
        return new IncomeTypeRepositoryJPA();
    }

    @Override
    public IPaymentMeanRepository getPaymentMeanRepository() {
        return new PaymentMeanRepositoryJPA();
    }
}






















/*
    @Override
    public IExpenseRepository iexpenseRepository() {
        return new Persistence.JPA.ExpenseRepositoryImpl();
    }

    @Override
    public TypeOfExpenseRepository TypeOfExpenseRepository() {
        return new Persistence.InMemory.TypeOfExpenseRepositoryImpl();
    }

    @Override
    public IncomeTypeRepository buildIncomeTypeRepository() {
        return new Persistence.InMemory.IncomeTypeRepositoryImpl();
    }

    @Override
    public IPaymentTypeRepository buildPaymentTypeRepository() {
        return Persistence.InMemory.PaymentTypeRepositoryImpl.getInstance();
    }

    @Override
    public IPayModeRepository buildPayModeRepository() {
        return Persistence.InMemory.PayModeRepositoryImpl.getInstance();
    }
}
*/
