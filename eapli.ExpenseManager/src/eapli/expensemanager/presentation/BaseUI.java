/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;

/**
 *
 * @author u
 */
public abstract class BaseUI {

	public abstract BaseController controller();

	public abstract boolean doShow();

	public void displayMonthValue() {
		double amount;

		amount = controller().getMonthlyExpenses();

		System.out.println("=============================");
		System.out.println(" Month Expenses:    " + amount);
		System.out.println("=============================\n");

	}

        public void displayWeekValue() {
            
            double amount = 0.0;
            amount = controller().getWeekExpenses();
            System.out.println("=============================");
            System.out.println(" Week Expenses:    " + amount);
            System.out.println("=============================");
    }

	public void mainLoop() {
		boolean wantsToExit;
		do {
			wantsToExit = show();
		} while (!wantsToExit);
	}

	public boolean show() {

		displayMonthValue();
		final boolean wantsToExit = doShow();

		return wantsToExit;
	}

}
