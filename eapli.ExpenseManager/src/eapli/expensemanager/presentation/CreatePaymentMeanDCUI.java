/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.RegisterPaymentMeanController;
import eapli.util.Console;

/**
 *
 * @author Vitor * Class que cria o meio de pagamento Cartão de debito Chamada
 * pela CreatePaymentMeanUI
 *
 */
public class CreatePaymentMeanDCUI extends BaseUI {

    private String CardName;
    private String CardNumber;

    RegisterPaymentMeanController controller = new RegisterPaymentMeanController();

    /**
     * inserir os dados para o cartao de debito
     */
//    private void show() {
//        CardName = Console.readLine("Enter card name >>");
//        CardNumber = Console.readLine("Enter card number >>");
//           }
//     /**
//     * Cria o controlador que faz a ponte da apresentação com o modelo
//     */
//    
//    private void submit() {
//        RegisterPaymentMeanController controller = new RegisterPaymentMeanController();
//        controller.CreatePaymentMeanControlerDC(CardName,CardNumber);
//    }
//
//    public void run() {
//        show();
//        submit();
//    }
    @Override
    public BaseController controller() {
        return controller;
    }

    @Override
    public boolean doShow() {
        CardName = Console.readLine("Enter card name >>");
        CardNumber = Console.readLine("Enter card number >>");
        controller.CreatePaymentMeanControlerDC(CardName, CardNumber);
        return true;
    }

}
