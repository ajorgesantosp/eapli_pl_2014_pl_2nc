/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.presentation;

import eapli.expensemanager.application.RegisterIncomeTypeController;
import eapli.util.Console;

/**
 * User interface para introduzir tipo de rendimento
 *
 * @author i101153
 */
public class RegisterIncomeTypeUI {

    /**
     * Declara variavel para guardar tipo de rendimento inserido
     */
    private String incomeType;

    /**
     * Prepara consola para ler tipo de rendimento
     */
    private void show() {
        incomeType = Console.readLine("Enter income type description >>");
    }

    /**
     * Pede ao controlador para criar novo tipo de rendimento
     */
    private void submit() {
        RegisterIncomeTypeController controller = new RegisterIncomeTypeController();
        controller.registerIncomeType(incomeType);
    }

    /**
     * Mostra a UI e guarda valor inserido pelo utilizador
     */
    public void run() {
        show();
        submit();
    }

}
