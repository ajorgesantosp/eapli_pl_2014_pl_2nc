/*

- * To change this license header, choose License Headers in Project Properties.

- * To change this template file, choose Tools | Templates

- * and open the template in the editor.

- */

package eapli.expensemanager.presentation;



import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.RegisterExpenseController;
import eapli.util.Console;
import java.util.Calendar;

/**

- *

- * @author Mak3r

- * Class created to handle the sub-menu of register expense. 

- * Called from MenuUI by selecting the option register expense.

- */

public class RegisterExpenseUI extends BaseUI  {

    private String expenseDesc;
    private Calendar expenseDate;
    private double expenseValue;
    private RegisterExpenseController controller = new RegisterExpenseController();

    /**
-     * parses the parameters of register expense
-     */
    @Override
    public BaseController controller() {

        return controller;

    }

	

    @Override
    public boolean doShow() {

        expenseDesc = Console.readLine("Enter expense description >>");

        expenseDate = Console.readCalendar("Enter the date when the expense was made (dd-mm-yyyy) >>");
        
        expenseValue = Console.readDouble("Enter the amount spent >>");

        controller.registerExpense(expenseDesc, expenseDate, expenseValue);

        return true;

    }

        

}

