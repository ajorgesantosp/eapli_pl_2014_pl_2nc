/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.util.Console;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 * based on Paulo Sousa code
 */
public class MenuUI extends BaseUI {

	private BaseController controller = new BaseController();

	@Override
	public BaseController controller() {
		return controller;
	}

	@Override
	public boolean doShow() {
		int option = -1;
		do {

			displayMonthValue();
                        displayWeekValue();
			System.out.println("=============================");
			System.out.println("          myMoney            ");
			System.out.println("=============================\n");
			System.out.println("--- Transactions ---");
			System.out.println("100. Register New Expense");
			System.out.println("101. Register New Income");
			System.out.println("--- Balances ---");
			System.out.println("200. Balances of Transactions");
			System.out.println("--- Master Tables ---");
			System.out.println("300. Register Expense Type");
			System.out.println("301. Register Income Type");
			System.out.println("302. Register Payment Mean");
			System.out.println("--- --- --- --- ---");
			System.out.println("0. Exit\n\n");

			option = Console.readInteger("Please choose an option");
			switch (option) {
				case 0:
					System.out.println("bye ...");
                                        break;

				case 100:
					final RegisterExpenseUI uc03 = new RegisterExpenseUI();
					uc03.show();
					break;
				case 101:
					final RegisterIncomeUI uc11 = new RegisterIncomeUI();
					uc11.show();
					break;
				case 300:
					final RegisterExpenseTypeUI uc01 = new RegisterExpenseTypeUI();
					uc01.show();
					break;
				case 301:
					final RegisterIncomeTypeUI uc10 = new RegisterIncomeTypeUI();
					uc10.run();
					break;
				case 302:
					final CreatePaymentMeanUI uc02 = new CreatePaymentMeanUI();
					uc02.show();
					break;
				default:
					System.out.println("option not recognized.");
					break;
			}
		} while (option != 0);
		return false;
	}
}
