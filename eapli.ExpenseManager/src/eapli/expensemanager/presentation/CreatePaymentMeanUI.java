/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.PaymentMeanQueryController;
import eapli.util.Console;

/**
 *
 * @author Vitor
 */
public class CreatePaymentMeanUI extends BaseUI {

//    public void run() {
//        
//    }
    private BaseController controller = new BaseController();

    @Override
    public BaseController controller() {
        return controller;
    }

    @Override
    public boolean doShow() {
        int option = -1;
        do {
            System.out.println("==========================================");
            System.out.println("          Create Payment Mean            ");
            System.out.println("==========================================\n");
            System.out.println("100. Credit Card");
            System.out.println("101. Debit Card");
            System.out.println("102. Check");
            System.out.println("-------------- Queries --------------------");
            System.out.println("200. Payments Means Querie");
            System.out.println("0. Main Menu\n\n");

            option = Console.readInteger("Please choose an option");
            switch (option) {
                case 0:
                    break;
                case 100:
                    final CreatePaymentMeanCCUI uc021 = new CreatePaymentMeanCCUI();
                    uc021.show();
                    break;
                case 101:
                    final CreatePaymentMeanDCUI uc022 = new CreatePaymentMeanDCUI();
                    uc022.show();
                    break;
                case 102:
                    final CreatePaymentMeanCheckUI uc023 = new CreatePaymentMeanCheckUI();
                    uc023.show();
                    break;
                case 200:
                    final PaymentMeanQueryController uc024 = new PaymentMeanQueryController();
                    uc024.PaymentMeanQueryController();
                    break;
                default:
                    System.out.println("option not recognized.");
                    break;
            }
        } while (option != 0);
        return false;
    }
}
