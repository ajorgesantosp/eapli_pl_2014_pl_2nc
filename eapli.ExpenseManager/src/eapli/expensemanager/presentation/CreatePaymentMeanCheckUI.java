/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.RegisterPaymentMeanController;
import eapli.util.Console;

/**
 *
 * @author Vitor * Class que cria o meio de pagamento Check Chamada pela
 * CreatePaymentMeanUI
 *
 */
public class CreatePaymentMeanCheckUI extends BaseUI {

    private String BankAcronym;
    private String BankName;

    RegisterPaymentMeanController controller = new RegisterPaymentMeanController();

    /**
     * inserir os dados para o cheque
     */
//    private void show() {
//        BankAcronym = Console.readLine("Enter bank acronym >>");
//        BankName = Console.readLine("Enter bank name >>");
//        AccountNumber = Console.readLine("Enter account number >>");
//           }
//    /**
//     * Cria o controlador que faz a ponte da apresentação com o modelo
//     */
//    
//    private void submit() {
//        RegisterPaymentMeanController controller = new RegisterPaymentMeanController();
//        controller.CreatePaymentMeanControlerCheck(BankAcronym, BankName, AccountNumber);
//    }
//
//    public void run() {
//        show();
//        submit();
//    }
    @Override
    public BaseController controller() {
        return controller;
    }

    @Override
    public boolean doShow() {
        BankAcronym = Console.readLine("Enter bank acronym >>");
        BankName = Console.readLine("Enter bank name >>");
        controller.CreatePaymentMeanControlerCheck(BankAcronym, BankName);
        return true;
    }

}
