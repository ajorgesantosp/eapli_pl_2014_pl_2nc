/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.RegisterIncomeController;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author sdcastro
 */
public class RegisterIncomeUI extends BaseUI {

    private String incomeDescription;
    private Calendar incomeDate;
    private double incomeValue;

    private RegisterIncomeController controller = new RegisterIncomeController();

    @Override
    public BaseController controller() {

        return controller;
    }

    @Override
    public boolean doShow() {

        incomeDescription = Console.readLine("Enter income description >>");
        incomeDate = Console.readCalendar("Enter the date income was received >>");
        incomeValue = Console.readDouble("Enter the amount >>");

        controller.registerIncome(incomeDescription, incomeDate, incomeValue);
        return true;
    }

}
