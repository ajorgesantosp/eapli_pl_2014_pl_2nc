/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.RegisterPaymentMeanController;
import eapli.util.Console;

/**
 *
 * @author Vitor
 *
 * Class que cria o meio de pagamento Cartão de Credito Called from MenuUI by
 * selecting the option register expense.
 *
 */
public class CreatePaymentMeanCCUI extends BaseUI {

    private String CardName;
    private String CardNumber;
    private RegisterPaymentMeanController controller = new RegisterPaymentMeanController();

//    /**
//     * inserir os dados para o cartao de credito
//     */
//    private void show() {
//        CardName = Console.readLine("Enter card name >>");
//        CardNumber = Console.readLine("Enter card number >>");
//    }

//    /**
//     * Cria o controlador que faz a ponte da apresentação com o modelo
//     */
//    private void submit() {
//        RegisterPaymentMeanController controller = new RegisterPaymentMeanController();
//        controller.CreatePaymentMeanControlerCC(CardName, CardNumber);
//    }

    @Override
    public BaseController controller() {
        return controller;
    }

    @Override
    public boolean doShow() {
        CardName = Console.readLine("Enter card name >>");
        CardNumber = Console.readLine("Enter card number >>");
        controller.CreatePaymentMeanControlerCC(CardName, CardNumber);

        return true;
    }

}
