/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.application;

import eapli.expensemanager.domain.Check;
import eapli.expensemanager.domain.CreditCard;
import eapli.expensemanager.domain.DebitCard;
import eapli.expensemanager.persistence.IPaymentMeanRepository;
import eapli.expensemanager.persistence.IRepositoryFactory;
import eapli.expensemanager.persistence.PersistenceFactory;

/**
 *
 * @author Vitor
 */
public class RegisterPaymentMeanController extends BaseController{

    public void CreatePaymentMeanControlerCC(String cardName, String cardNumber) {

        CreditCard creditCard = new CreditCard(cardName, cardNumber);

        IRepositoryFactory repoFactory = PersistenceFactory.getInstance().getRepositoryFactory();

        IPaymentMeanRepository repo = repoFactory.getPaymentMeanRepository();

       // repo.save(creditCard);

    }

    public void CreatePaymentMeanControlerDC(String cardName, String cardNumber) {

        DebitCard debitCard = new DebitCard(cardName, cardNumber);

        IRepositoryFactory repoFactory = PersistenceFactory.getInstance().getRepositoryFactory();

        IPaymentMeanRepository repo = repoFactory.getPaymentMeanRepository();

       // repo.save(debitCard);
    }

    public void CreatePaymentMeanControlerCheck(String banckAcronym, String bankName) {

        Check check = new Check(banckAcronym, bankName);

        IRepositoryFactory repoFactory = PersistenceFactory.getInstance().getRepositoryFactory();

        IPaymentMeanRepository repo = repoFactory.getPaymentMeanRepository();
        
       // repo.save(check);
    }

}
