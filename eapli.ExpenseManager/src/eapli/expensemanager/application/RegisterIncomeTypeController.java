/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.application;

import eapli.expensemanager.domain.IncomeType;
import eapli.expensemanager.persistence.IIncomeTypeRepository;
import eapli.expensemanager.persistence.IRepositoryFactory;
import eapli.expensemanager.persistence.PersistenceFactory;

/**
 * Controlador para registar tipos de rendimento
 *
 * @author i101153
 */
/* UC5 Fizemos o Extends do  BaseUIController */
public class RegisterIncomeTypeController extends BaseController {

    public void registerIncomeType(String incomeTypeText) {

        /**
         * Cria nova instância do tipo IncomeType com a descrição passada por
         * argumento
         */
        IncomeType incomeType = new IncomeType(incomeTypeText);
        
        /**
         * Pede à fábrica de repositório para indicar qual o repositório
         * para armazenar os dados
         */
        
        IRepositoryFactory repoFactory = PersistenceFactory.getInstance().getRepositoryFactory();
        
        IIncomeTypeRepository repo = repoFactory.getIncomeTypeRepository();
        //IncomeTypeRepository repo = repoFactory.getIncomeTypeRepository();
        
        /**
         * Guarda os dados no repositório.
        */
        
        repo.save(incomeType);

    }

}
