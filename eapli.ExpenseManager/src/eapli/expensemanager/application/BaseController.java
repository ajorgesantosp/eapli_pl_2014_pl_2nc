/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.application;
import eapli.expensemanager.persistence.Book;
import eapli.expensemanager.persistence.IMonthlySheetRepository;
import eapli.expensemanager.persistence.IRepositoryFactory;
import eapli.expensemanager.persistence.PersistenceFactory;
import eapli.util.DateTime;
/**
 *
 * @author u
 */
public class BaseController {
    
    /* Método para obter as despesas mensais*/
    public double getMonthlyExpenses(){
        double amount;
        Book book = new Book();
        amount = book.findMonthlySheet(DateTime.currentMonth());
        return amount;
    }

    public double getWeekExpenses(){
        double amount;
        IRepositoryFactory repoFactory = PersistenceFactory.getInstance().getRepositoryFactory();
        IMonthlySheetRepository repo = repoFactory.getMonthlySheetRepository();
                
        amount = repo.calculateWeekExpenses();
        return amount;
    }
}
