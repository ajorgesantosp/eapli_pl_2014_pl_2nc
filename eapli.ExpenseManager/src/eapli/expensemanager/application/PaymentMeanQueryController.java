/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.application;

import eapli.expensemanager.domain.PaymentMean;
import eapli.expensemanager.persistence.IPaymentMeanRepository;
import eapli.expensemanager.persistence.IRepositoryFactory;
import eapli.expensemanager.persistence.InMemory.PaymentMeanRepositoryImpl;
import eapli.expensemanager.persistence.PersistenceFactory;
import java.util.ArrayList;
import java.util.Iterator;



/**
 *
 * @author Helder
 */
public class PaymentMeanQueryController{

    /**
     * Implementação da listagem dos meus de pagamentos carregados
     */
    
    public void PaymentMeanQueryController() {
        IRepositoryFactory repoFactory = PersistenceFactory.getInstance().getRepositoryFactory();
        IPaymentMeanRepository repo = repoFactory.getPaymentMeanRepository();
        
        ArrayList<PaymentMean> pm = repo.retrievePaymentMeans();
        
        System.out.println("----------------------------- Payments Means Querie -----------------------------");
                
        for (PaymentMean a : pm) {
            System.out.println(a.toString());
        }
        System.out.println("---------------------------------------------------------------------------------");
    }
}


