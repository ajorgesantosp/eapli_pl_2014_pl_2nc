/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.application;

import eapli.expensemanager.domain.Expense;
import eapli.expensemanager.domain.MonthlySheet;
import eapli.expensemanager.domain.PaymentMean;
import eapli.expensemanager.persistence.Book;
import eapli.expensemanager.persistence.IMonthlySheetRepository;
import eapli.expensemanager.persistence.IPaymentMeanRepository;
import eapli.expensemanager.persistence.IRepositoryFactory;
import eapli.expensemanager.persistence.PersistenceFactory;
import eapli.expensemanager.persistence.jpa.MonthlySheetRepositoryJPA;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Mak3r
 * Class controller that handles the bridge between presentation and model package
 */
public class RegisterExpenseController extends BaseController {

    /**
     * 
     * @param expenseDesc saves the description of the expense
     * @param expenseDate saves the date the expense was made
     * @param expenseValue  saves the amount spent
     * 
     * method called by the sub-menuui to bridge presentation and domain.
     * creates an expense object with the given parameters, 
     * calls the book to find the correct monthlysheet, or creates if non-existant
     * and adds the new expense to the motnhlysheet
     */
    public void registerExpense(String expenseDesc, Calendar expenseDate, double expenseValue) {
        
        Expense expense = new Expense(expenseDesc, expenseDate, expenseValue);
    
       IRepositoryFactory repoFactory = PersistenceFactory.getInstance().getRepositoryFactory();
       IMonthlySheetRepository repo = repoFactory.getMonthlySheetRepository();

        
        MonthlySheet monthlysheet2 = repo.findSheet(expense.getMonthFromExpense());
        
        monthlysheet2.registerExpense(expense);

       
        repo.save(monthlysheet2);
  
         
        
    }
    public ArrayList<PaymentMean> listPaymentMeans(){
        IRepositoryFactory repoFactory = PersistenceFactory.getInstance().getRepositoryFactory();
        IPaymentMeanRepository repo = repoFactory.getPaymentMeanRepository();
        return repo.retrievePaymentMeans();
    }
}
