/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.application;

import eapli.expensemanager.domain.ExpenseType;
import eapli.expensemanager.persistence.IExpenseTypeRepository;
import eapli.expensemanager.persistence.IRepositoryFactory;
import eapli.expensemanager.persistence.PersistenceFactory;


/* UC5 FAzer o Extends do  BaseUIController */
public class RegisterExpenseTypeController extends BaseController {

	public void registerExpenseType(String expenseTypeText) {
		ExpenseType expenseType = new ExpenseType(expenseTypeText);

		IRepositoryFactory repoFactory = PersistenceFactory.getInstance().getRepositoryFactory();

		IExpenseTypeRepository repo = repoFactory.getExpenseTypeRepository();

		repo.save(expenseType);
	}

}
