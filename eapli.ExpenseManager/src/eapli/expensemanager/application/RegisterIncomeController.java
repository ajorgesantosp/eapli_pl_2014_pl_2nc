/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.application;

import eapli.expensemanager.domain.Income;
import eapli.expensemanager.domain.MonthlySheet;
import eapli.expensemanager.persistence.IMonthlySheetRepository;
import eapli.expensemanager.persistence.IRepositoryFactory;
import eapli.expensemanager.persistence.PersistenceFactory;
import java.util.Calendar;

/**
 *
 * @author sdcastro
 */
public class RegisterIncomeController extends BaseController {

    public void registerIncome(String incomeDescription, Calendar incomeDate, double incomeValue) {
        
        Income income = new Income(incomeDescription, incomeDate, incomeValue);

        IRepositoryFactory repoFactory = PersistenceFactory.getInstance().getRepositoryFactory();
        IMonthlySheetRepository repo = repoFactory.getMonthlySheetRepository();

        
        MonthlySheet ms = repo.findSheet(income.getMonthFromIncome());
        ms.registerIncome(income);
        repo.save(ms);
        

    }

}
